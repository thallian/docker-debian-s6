FROM debian:stable-slim
MAINTAINER Sebastian Hugentobler <sebastian@vanwa.ch>

ENV S6_OVERLAY_VERSION=v1.20.0.0

RUN apt-get -y update && \
    apt-get -y install syslog-ng wget && \
    wget -qO- https://github.com/just-containers/s6-overlay/releases/download/$S6_OVERLAY_VERSION/s6-overlay-amd64.tar.gz | tar -xz -C / && \
    apt-get -y remove --purge --auto-remove wget

ADD /rootfs /

ENTRYPOINT ["/init"]
